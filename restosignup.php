<!-- Code for Inserting Restaurants Details in Database  Starts -->
<?php
  $error='';
  require 'connection.php';
   $conn = Connect();
   if($_SERVER["REQUEST_METHOD"]=="POST"){
      
    $fullname = $conn->real_escape_string($_POST['fullname']);
    $username = $conn->real_escape_string($_POST['username']);
    $email = $conn->real_escape_string($_POST['email']);
    $contact = $conn->real_escape_string($_POST['contact']);
    $address = $conn->real_escape_string($_POST['address']);
    $password = $conn->real_escape_string($_POST['password']);

     $sqlUser="Select * from restaurants where username='$username'";
          $result1 = mysqli_query($conn, $sqlUser);
          if (mysqli_num_rows($result1) > 0)
          {
             $error='Username already Exist'; //To check that username already Exists
            
          }
          else{
            $query = "INSERT into restaurants(fullname,username,email,contact,address,password) VALUES('" . $fullname . "','" . $username . "','" . $email . "','" . $contact . "','" . $address ."','" . $password ."')";
            $register = $conn->query($query);
                       if (!$register){
	      die("Couldnt enter data: ".$conn->error);
              }else{
                  header("location: resto_registered_success.php"); 
              }
          }
          $conn->close(); //connection close
   }  
?>
<!-- Code for Database Connection Ends -->

<!-- Html Code Starts -->
<html>
     <head>
        <title> Resturant Signup | The Sassy Spoon</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/usersignup.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script</script>
    </head> 

   <body>
     <!-- Scroll bar function starts from here -->
          <script type="text/javascript">
                window.onscroll = function() 
                   {
                     scrollFunction()
                  };
                  function scrollFunction(){
                       if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                           document.getElementById("myBtn").style.display = "block";
                      } else {
                           document.getElementById("myBtn").style.display = "none";
                        }
                     }
                     function topFunction() {
                          document.body.scrollTop = 0;
                         document.documentElement.scrollTop = 0;
                      }
                  function validate1(){
                       if(signupform_resto.contact.value.length!=10){  //Check that mobile number should not be les than 10 digit
                           alert("Mobile number must be of 10 digits");
                           return false;
                           }
                           }
             </script>
    <!-- Script Tag End -->

 <!-- Header Starts here -->

    <nav class="navbar navbar-inverse navbar-fixed-top navigation-clean-search" role="navigation">
          <div class="container">
                <div class="navbar-header">
                        <a class="navbar-brand" href="">The Sassy Spoon</a>
                </div>
                <div class="collapse navbar-collapse " id="myNavbar">
                    <ul class="nav navbar-nav">
                         <li class="active" ><a href="index.php">Home</a></li>
                         <li><a href="foodmenu.php"><span class="glyphicon glyphicon-cutlery"></span> Food Menu </a></li>
                    </ul>
                     <!-- Dropdown for signUp form both User and Restaurants -->
                    <ul class="nav navbar-nav navbar-right">
                       <li>
                               <a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Sign Up <span class="caret"></span> </a>
                            <ul class="dropdown-menu">
                                    <li><a href="usersignup.php"> User Sign-up</a></li>
                                    <li><a href="restosignup.php"> Resturant Sign-up</a></li>
                            </ul>
                      </li>
                        <!-- Dropdown for signUp form both User and Restaurants Ends-->

                     <!-- Dropdown for Login Form form both User and Restaurants -->
                      <li>
                          <a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-log-in"></span> Login <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="userlogin.php"> User Login</a></li>
                                <li><a href="restologin.php"> Resturant Login</a></li>
                            </ul>
                     </li>
                     <!-- Dropdown for Login Form form both User and Restaurants Ends -->
                    </ul>
              </div>

        </div>
   </nav>
 <!-- Header End here -->

 <!-- Message div start here -->
       <div style="text-align:center; padding: 35px; font-family: latha;  color: white;">
            <h1>Hi Manager! <br> Welcome to The Sassy Spoon</h1>
            <br>
            <p>Get started by creating your account</p>
       </div>
<!-- Message div ends here -->

       <!-- SignUp form Start from Here -->
           <div id="main-wrapper">
               <center>
               <h3 style="color:red"><b>Registration Form</h3>
                   <img src='./images/login.png' class="loginicon"></img>
               </center>
               <form class="myForm" name="signupform_resto" action="" onsubmit="return validate1()" method="post">
                    <label style="margin-left: 5px;color: red;"><span> <?php echo $error;  ?> </span></label><br>

                   <label for="fullname"><b><span class="text-danger" style="margin-right: 5px;">*</span> Fullname:</label><br>
                   <input name="fullname" type="text" class="inputvalue" placeholder="Enter your name" required/><br>

                   <label for="username"><b><span class="text-danger" style="margin-right: 5px;">*</span> Username:</label><br>
                   <input name="username" type="text" class="inputvalue" placeholder="Enter your name" required/><br>

                   <label for="email"><b><span class="text-danger" style="margin-right: 5px;">*</span> Email:</label><br>
                   <input name="email" type="email" class="inputvalue" placeholder="Enter your email" required/><br>

                   <label for="contact"><b><span class="text-danger" style="margin-right: 5px;" >*</span> Contact:</label><br>
                   <input name="contact" type="text" class="inputvalue" onKeyPress="if(this.value.length===10) return false;" placeholder="Enter your contact number" required/><br>

                   <label for="address"><b><span class="text-danger" style="margin-right: 5px;">*</span> Address:</label><br>
                   <input name="address" type="text" class="inputvalue" placeholder="Enter your address" required/><br>

                   <label for="password"><b><span class="text-danger" style="margin-right: 5px;">*</span> Password:</label><br>
                   <input name="password" type="password" class="inputvalue" placeholder="Enter your password" required/><br>

                   <!-- <label><b>Confirm Password</label><br>
                   <input type="text" class="inputvalue" placeholder="Confirm password"/><br> -->

                   <input type="submit" id="signup_btn" value="Sign Up"/><br>
                   <a href="restologin.php"> <input type="button" id="back_btn" value="Back"/></a>

               </form>
            </div> 
        <!--Signup form end here  --> 
<!-- Footer -->
      <footer class="page-footer font-small blue" style="background: black;margin-top:2rem;color: white">
<!-- Copyright -->
            <!-- <div class="footer-copyright text-center py-3">&copy; 2020 Copyright:
                The Saasy Spoon
            </div> -->
<!-- Copyright -->
      </footer>
      <!-- Footer -->
  </body>
</html>