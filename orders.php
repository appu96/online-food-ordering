<?php
session_start();
require 'connection.php';
$conn = Connect();
if(!isset($_SESSION['login_user2'])){
header("location: userlogin.php"); 
}
$food_name;
$food_price;
$c_id;
?>

<html>
    <head>
        <title> Order | The Sassy Spoon</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/orders.css"> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script</script>
    </head>
  <body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <!-- <a class="navbar-brand" href="#">The Sassy Spoon</a> -->
        <ul class="navbar-nav">
            <li class="nav-item">
            <!-- <a class="nav-link" href="home">Home</a> -->
        </li>
    </ul>

        <?php
        if(isset($_SESSION['login_user2'])){
        ?>
            <ul class="nav navbar-nav navbar-right" style="position: absolute;right:1rem">
                <li style="margin-right:1rem;"><a href="#" style="color: white"> Welcome <?php echo $_SESSION['login_user2']; ?> </a></li>
                <li><a href="logout_user.php" style="color:white">Log Out </a></li>
            </ul>
        <?php
        }
        ?>

    </nav>

    <?php   
        $F_ID= $_GET['id'];
        $order_id;
        $sqlFood = "SELECT * FROM food WHERE food_id = '$F_ID' ";
        $resultFood = mysqli_query($conn, $sqlFood);
        $user_id=0;
      
        if (mysqli_num_rows($resultFood) > 0)
        {
          while($rowFood = mysqli_fetch_assoc($resultFood)){
           
            $food_name = $rowFood["food_name"];
            $food_price =  $rowFood["food_price"];
         }
      }
         
            $username = $_SESSION["login_user2"];
            $sqlUId = "SELECT user_id FROM user WHERE username = '$username' ";
            $resultUID = mysqli_query($conn, $sqlUId);
            
            if (mysqli_num_rows($resultUID) > 0)
              {
                while($rowUId = mysqli_fetch_assoc($resultUID)) {
                   
                     $c_id= $rowUId["user_id"];
         }
      }
      
      
        $quantity = $_POST['quantity'];
        $R_ID = $_GET['Rid'];
        $order_date = date('Y-m-d');


     $query = "INSERT INTO orders (food_id, foodname, food_price,  quantity, order_date, user_id, resto_id) 
              VALUES ('" . $F_ID . "','" . $food_name . "','" . $food_price . "','" . $quantity . "','" . $order_date . "','" . $c_id . "','" . $R_ID . "')";
             

              $success = $conn->query($query);  
              
      if(!$success)
      {
        ?>
        <div style="text-align: center;">
          <div class="]">
            <h1>Something went wrong!</h1>
            <p>Try again <a href="foodmenu.php">Order Now</a>.</p>
          </div>
        </div>

        <?php
      }else{
            $sqlOrderId = "SELECT * FROM orders Order by order_id DESC LIMIT 1";
            $resultOrderId = mysqli_query($conn, $sqlOrderId);
            if (mysqli_num_rows($resultOrderId) > 0)
             {
              while($rowOrderId = mysqli_fetch_assoc($resultOrderId)){
                     $order_id= $rowOrderId['order_ID'];
              } 
           }
           
          ?>
            <div style="margin: auto;width:50%;text-align: center;margin-top:2rem ">
            <div class="wrap-item card">
                <h3>Thank you for Order! We received your order!</h3>
                <h3>Food is currently being prepared.Your Order Id is: <?php echo $order_id?></h3>
                <h5>Please keep your cash ready! We will keep you updatesd as soon as your food is on the way</h5>
                <h4>You can order more food...<a href="foodmenu.php">Order Now</a>.</h4>
            </div> 
          </div>  
        <?php
          
      }
      
  ?>
    </body>
</html>