<?php
include('login_user.php'); 

if(isset($_SESSION['login_user2'])){
header("location: foodmenu.php"); 
}
?>

<!-- HTml Code starts here -->
<html>
    <head>
        <title>User Login | The Sassy Spoon </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/usersignup.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script</script>
    </head> 
    <body>
       <!-- Header Starts here -->
       
       <nav class="navbar navbar-inverse navbar-fixed-top navigation-clean-search" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                                <a class="navbar-brand" href="">The Sassy Spoon</a>
                     </div>
                   <div class="collapse navbar-collapse " id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li class="active" ><a href="index.php">Home</a></li>
                            <li><a href="foodmenu.php"><span class="glyphicon glyphicon-cutlery"></span> Food Menu </a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Sign Up <span class="caret"></span> </a>
                                <ul class="dropdown-menu">
                                    <li><a href="usersignup.php"> User Sign-up</a></li>
                                    <li><a href="restosignup.php"> Resturant Sign-up</a></li>
                                </ul>
                            </li>
                            
                            <li>
                               <a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-log-in"></span> Login <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                   <li><a href="userlogin.php"> User Login</a></li>
                                   <li><a href="restologin.php"> Resturant Login</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
         </nav>
 <!-- Header End here -->

 <!-- Message div ends here -->
       <div style="text-align:center; padding: 35px; font-family: latha;  color: white;">
            <h1>Hi Guest, <br> Welcome to The Sassy Spoon</h1>
            <br>
            <p>Kindly Login to continue</p>
       </div>
<!-- Message div ends here -->

       <!-- Login Form Start from Here -->
           <div id="main-wrapper">
               <center>
               <h3 style="color:red"><b>Login Form</h3>
                   <img src='./images/login.png' class="loginicon"></img>
               </center>
               <form class="myForm" action="" method="post">
                   <label for="username"><b><span class="text-danger" style="margin-right: 5px;">*</span> Username:</label><br>
                   <input name="username" type="text" class="inputvalue" placeholder="Enter your name" required/><br>

                   <label for="password"><b><span class="text-danger" style="margin-right: 5px;">*</span> Password:</label><br>
                   <input name="password" type="password" class="inputvalue" placeholder="Enter your password" required/><br>

                   <label style="margin-left: 5px;color: red;"><span> <?php echo $error;  ?> </span></label>
                   <input name="login" type="submit" id="login_btn" value="Login"/><br>
                   <a href="usersignup.php"><input type="button" id="register_btn" value="Register"/></a>

               </form>
            </div> 
          
        <!--login Form end here  -->
    
    
  </body>
</html>