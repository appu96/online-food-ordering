<html>
<head>
    <title> User registered | The Sassy Spoon </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/usersignup.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script</script>
</head> 
<body>
<!-- Header Start from here -->

<nav class="navbar navbar-inverse navbar-fixed-top navigation-clean-search" role="navigation">
          <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar">
                           <span class="sr-only">Toggle navigation</span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                    </button>
                        <a class="navbar-brand" href="">Cafe Peter'</a>
                </div>
          <div class="collapse navbar-collapse " id="myNavbar">

                <ul class="nav navbar-nav">
                    <li class="active" ><a href="index.php">Home</a></li>
                    <li><a href="foodmenu.php"><span class="glyphicon glyphicon-cutlery"></span> Food Menu </a></li>
                </ul>


            <ul class="nav navbar-nav navbar-right">
                 <li>
                     <a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Sign Up <span class="caret"></span> </a>
                        <ul class="dropdown-menu">
                           <li><a href="usersignup.php"> User Sign-up</a></li>
                           <li><a href="restosignup.php"> Resturant Sign-up</a></li>
                        </ul>
                 </li>

                  <li>
                      <a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-log-in"></span> Login <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                             <li><a href="userlogin.php"> User Login</a></li>
                             <li><a href="restologin.php"> Resturant Login</a></li>
                        </ul>
                  </li>

            </ul>
       </div>

    </div>
 </nav>
 <!-- Header ends here -->

<!-- Message of successfully registered  -->
       <div style="text-align:center; padding: 35px; font-family: latha;  color: white;">
            <h1><?php echo "Welcome Guest!" ?><br> Your account has been successfully Created! </span></h1>
            <br>
            <h3>Login Now from <a href="userlogin.php">  HERE</a></h3>
       </div>
<!-- Message div end here -->
</body>
</html>