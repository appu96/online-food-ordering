<!-- Code for Inserting Customer Detail into Database -->
<?php
  $error='';
  require 'connection.php';
   $conn = Connect();
   if($_SERVER["REQUEST_METHOD"]=="POST"){
      
    $fullname = $conn->real_escape_string($_POST['fullname']);
    $username = $conn->real_escape_string($_POST['username']);
    $email = $conn->real_escape_string($_POST['email']);
    $contact = $conn->real_escape_string($_POST['contact']);
    $address = $conn->real_escape_string($_POST['address']);
    $password = $conn->real_escape_string($_POST['password']);
    $food_pref = $conn->real_escape_string($_POST['food_pref']);


     $sqlUser="Select * from user where username='$username'";
          $result1 = mysqli_query($conn, $sqlUser);
          if (mysqli_num_rows($result1) > 0) // Check whether username already exist or not
          {
             $error='Username already Exist';
            
          }
          else{
            $query = "INSERT into user(fullname,username,email,contact,address,password,food_pref) VALUES('" . $fullname . "','" . $username . "','" . $email . "','" . $contact . "','" . $address ."','" . $password ."', '" . $food_pref ."')";
            $register = $conn->query($query);
                       if (!$register){
	      die("Couldnt enter data: ".$conn->error);
              }else{
                  header("location: user_registered_success.php"); // redirected to next page
              }
          }
          $conn->close();
   }
?>
<!-- Code for Inserting Customer Detail into Database -->
<!-- Html code start from here -->
<html>
    <head>
        <title> User Signup | The Sassy Spoon </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/usersignup.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script</script>
    </head> 

    <body>
     <!-- Scroll bar function starts from here -->
      
      <script type="text/javascript">
                window.onscroll = function()
                {
                    scrollFunction()
                };

                function scrollFunction(){
                    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    document.getElementById("myBtn").style.display = "block";
                    } else {
                    document.getElementById("myBtn").style.display = "none";
                    }
                }

                function topFunction() {
                    document.body.scrollTop = 0;
                    document.documentElement.scrollTop = 0;
                }
                function validate(){
                       if(signupform.preference.value == '') {
                            alert("Select Preference for food"); // Food prefrence for food must be selected
                            return false;
                            }
                            if(signupform.contact.value.length!=10){
                            alert("Mobile number must be of 10 digits"); // mobile number validation
                            return false;
                        }
             
        
                  }
         </script>
    <!-- Scrollbar function end here -->

  <!-- Header Starts here -->
    <nav class="navbar navbar-inverse navbar-fixed-top navigation-clean-search" role="navigation">
          <div class="container">
                <div class="navbar-header">
                        <a class="navbar-brand" href="">The Sassy Spoon</a>
                 </div>
                 <div class="collapse navbar-collapse " id="myNavbar">
                     <ul class="nav navbar-nav">
                            <li class="active" ><a href="index.php">Home</a></li>
                            <li><a href="foodmenu.php"><span class="glyphicon glyphicon-cutlery"></span> Food Menu </a></li>
                      </ul>
                       <!-- Dropdown for signUp form both User and Restaurants -->
                      <ul class="nav navbar-nav navbar-right">
                           <li>
                               <a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Sign Up <span class="caret"></span> </a>
                             <ul class="dropdown-menu">
                                <li><a href="usersignup.php"> User Sign-up</a></li>
                                <li><a href="restosignup.php"> Resturant Sign-up</a></li>
                              </ul>
                          </li>
                          <!-- Dropdown for signUp form both User and Restaurants Ends-->

                          <!-- Dropdown for Login Form form both User and Restaurants -->
                             <li>
                                <a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-log-in"></span> Login <span class="caret"></span></a>
                                  <ul class="dropdown-menu">
                                        <li><a href="userlogin.php"> User Login</a></li>
                                        <li><a href="restologin.php"> Resturant Login</a></li>
                                  </ul>
                             </li>
                        </ul>
                 </div>

            </div>
   </nav>
         <!-- Header End here -->

<!-- Message div starts from here -->
       <div style="text-align:center; padding: 35px;  font-family: latha;  color: white; ">
            <h1>Hi Guest, <br> Welcome to The Sassy Spoon</h1>
            <br>
            <p>Get started by creating your account</p>
       </div>
<!-- Message div ends here -->

<!-- SignUp Form Start from Here -->
           <div id="main-wrapper">
               <center>
                   <h3 style="color:red"><b>Registration Form</h3>
                   <img src='./images/login.png' class="loginicon"></img>
               </center>
                <form class="myForm" name="signupform" action="" onsubmit="return validate()" method="post">
                        <label style="margin-left: 5px;color: red;"><span> <?php echo $error;  ?> </span></label><br>

                        <label for="fullname"><b><span class="text-danger" style="margin-right: 5px;">*</span> Fullname:</label><br>
                        <input name="fullname" type="text" class="inputvalue" placeholder="Enter your name" required/><br>

                        <label for="username"><b><span class="text-danger" style="margin-right: 5px;">*</span> Username:</label><br>
                        <input name="username" type="text" class="inputvalue" placeholder="Enter username" required/><br>

                        <label for="email"><b><span class="text-danger" style="margin-right: 5px;">*</span> Email:</label><br>
                        <input name="email" type="email" class="inputvalue" placeholder="Enter your email" required/><br>

                        <label for="contact"><b><span class="text-danger" style="margin-right: 5px;">*</span> Contact:</label><br>
                        <input name="contact" type="text"  class="inputvalue" onKeyPress="if(this.value.length===10) return false;" placeholder="Enter your contact number" required/><br>

                        <label for="address"><b><span class="text-danger" style="margin-right: 5px;">*</span> Address:</label><br>
                        <input name="address" type="text" class="inputvalue" placeholder="Enter your address"required/><br>

                        <label for="password"><b><span class="text-danger" style="margin-right: 5px;">*</span> Password:</label><br>
                        <input name="password" type="password" class="inputvalue"  placeholder="Enter your password" required/><br>

                        <span class="text-danger" style="margin-right: 5px;">*</span>Select food Category:
                        <input type="radio" name="food_pref" value="veg"> Veg
                        <input type="radio" name="food_pref" value="non-veg"> Non-veg<br>

                        <input name="submit_btn" type="submit" id="signup_btn" value="Sign Up"/><br>
                        <a href= "userlogin.php"><input type="button" id="back_btn" value="Back"/></a>
  
               </form>
            </div> 
        <!--Signup Form end here  -->
  
    
  </body>
</html>