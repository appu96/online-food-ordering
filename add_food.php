<?php
include('session_resto.php');

if(!isset($login_session)){
header('Location: restologin.php'); 
}

?>
<!DOCTYPE html>
<html>
    <head>
        <title> Add Food |The Sassy Spoon</title>
        <link rel="stylesheet" type = "text/css" href ="css/add_food.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script</script>
    </head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top navigation-clean-search" role="navigation">
         <div class="container">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
              </button>
               <a class="navbar-brand" href="index.php" style="color:white;">The Saasy Spoon</a>
            </div>
            <div class="collapse navbar-collapse " id="myNavbar">
               <ul class="nav navbar-nav">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="foodmenu.php"><span class="glyphicon glyphicon-cutlery"></span> Food Menu </a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" style="color:white;text-transform: capitalize;">Welcome <?php echo $login_session; ?> </a></li>
                    <li><a href="logout_resto.php">Log Out </a></li>
                </ul>
            </div>
        </div>
    </nav>
       <!-- Form For Adding food Starts here -->

      <div class="" style="padding: 0px 100px ;position:absolute;top:25%;left:35%;background: white;border: 2px solid white">
         <form action="add_foodresto.php" method="POST">
            <br style="clear: both">
             <h3 style="margin-bottom: 25px; text-align: center; font-size: 30px;color: blue;"> ADD NEW FOOD ITEM HERE </h3>

          <div class="form-group">
            <label><b><span class="text-danger" style="margin-right: 5px;">*</span> Food name:</label><br>
            <input type="text" class="form-control" id="food_name" name="food_name" placeholder="Enter Food name" required="">
          </div>     

          <div class="form-group">
            <label><b><span class="text-danger" style="margin-right: 5px;">*</span>Food Price</label><br>
            <input type="text" class="form-control" id="food_price" name="food_price" placeholder="Enter Food Price (INR)" required="">
          </div>

          <div class="form-group">
            <label><b><span class="text-danger" style="margin-right: 5px;">*</span> Food Description:</label><br>
            <input type="text" class="form-control" id="food_desc" name="food_desc" placeholder="Enter Food Description" required="">
          </div>

          <div class="form-group">
            <label><b><span class="text-danger" style="margin-right: 5px;">*</span>Image:</label><br>
            <input type="text" class="form-control" id="images_path" name="images_path" placeholder="Enter Food Image Path [./images/filename.extention]" required="">
          </div>
          <div class="form-group col-xs-12">
                <label><span class="text-danger" style="margin-right: 5px;">*</span>Select Food Preference: </label>
                <div class="custom-control custom-radio">
                  <input type="radio" id="customRadio1" name="food_pref" value="Veg" class="custom-control-input">
                  <label class="custom-control-label" for="customRadio1" required>Veg</label>
        </div>
      <div class="custom-control custom-radio">
           <input type="radio" id="customRadio2" name="food_pref" value="Non-Veg" class="custom-control-input">
           <label class="custom-control-label" for="customRadio2" required>Non-Veg</label>
       </div> 
            <br>
          <div class="form-group">
              <button type="submit" id="submit" name="submit" class="btn" style="background-color:blue;color: white"> ADD FOOD </button>    
          </div>
        </form>
        <!-- Forms End Here -->
    </div>

  </body>
</html>