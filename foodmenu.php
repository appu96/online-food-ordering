<?php
session_start();
require 'connection.php';
$conn = Connect();

?>
<html>

    <head>
        <title> Explore | Food The Sassy Spoon</title>
        <link rel="stylesheet" type = "text/css" href ="css/foodlist.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script</script>
    </head>
  <body>
      <!-- Script tag  -->
    <script type="text/javascript">
      window.onscroll = function()
      {
        scrollFunction()
      };

      function scrollFunction(){
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
          document.getElementById("myBtn").style.display = "block";
        } else {
          document.getElementById("myBtn").style.display = "none";
        }
      }

      function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
      }
    </script>
    <!-- Script tag close -->

    <nav class="navbar navbar-inverse navbar-fixed-top navigation-clean-search" role="navigation">
      <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php">The Sassy Spoon</a>
                </div>
          <div class="collapse navbar-collapse " id="myNavbar">
                <ul class="nav navbar-nav">
                    <li><a href="index.php">Home</a></li>
                </ul>

             <?php
             if(isset($_SESSION['login_user1'])){

                ?>
                <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Welcome <?php echo $_SESSION['login_user1']; ?> </a></li>
                        <li><a href="view_order.php"><span class="glyphicon glyphicon-log-out"></span> View Order</a></li>
                        <li><a href="add_food.php"><span class="glyphicon glyphicon-log-out"></span>Add Food</a></li>
                        <li><a href="logout_resto.php"><span class="glyphicon glyphicon-log-out"></span> Log Out </a></li>
                 </ul>
             <?php
                            }
                else if (isset($_SESSION['login_user2'])) {
                ?>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#"><span class="glyphicon glyphicon-user"></span> Welcome <?php echo $_SESSION['login_user2']; ?> </a></li>
                            <!-- <li class="active" ><a href="foodlist.php"><span class="glyphicon glyphicon-cutlery"></span> Food Zone </a></li> -->
                            <li><a href="user_orders.php"><span class="glyphicon glyphicon-shopping-cart">Your Order</span>
                            <li><a href="logout_user.php"><span class="glyphicon glyphicon-log-out"></span> Log Out </a></li>
                        </ul>
                <?php      
                }
          else {

          ?>

         <ul class="nav navbar-nav navbar-right">
               <li><a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Sign Up <span class="caret"></span> </a>
                    <ul class="dropdown-menu">
                        <li> <a href="usersignup.php"> User Sign-up</a></li>
                        <li> <a href="managersignup.php"> Manager Sign-up</a></li>
                    
                    </ul>
                </li>

                <li><a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-log-in"></span> Login <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li> <a href="userlogin.php"> User Login</a></li>
                    <li> <a href="restologin.php"> Manager Login</a></li>

                </ul>
               </li>
        </ul>

        <?php
        }
        ?>
        
       </div>
    </div>
</nav>
<!-- Image slider div starts -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
    <div class="carousel-inner">
            <div class="item active">
                <img src="./images/vegthai.jpg" style="width:100%; height: 50rem;">
               <div class="carousel-caption"></div>
           </div>

           <div class="item">
                <img src="./images/vegdosa.jpg" style="width:100%; height: 50rem;">
                <div class="carousel-caption"></div>
          </div>
          <div class="item">
                <img src="./images/veggolgappe.jpg" style="width:100%; height: 50rem;">
                <div class="carousel-caption"></div>
           </div>
    
    </div>
     <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
     </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!-- IMage slider div end -->

<!-- Message div starts -->
       <div style="text-align:center; padding: 15px; font-family: latha;  color: black;">
            <h1><?php echo "Welcome!" ?><br> Your can explore food here!</span></h1>
            <br>
       </div>
<!-- Message div ends here -->
<div class="container" style="width:95%;">
      <!-- Display all Food from food table -->
         <div class="container" style="width:95%;">
                <?php
                $sql;
                $result;
                $foodCount=0;
                $foodCountRes=0;
                $sqlFoodcount="Select * from food ";
                    $resultFoodCount = mysqli_query($conn, $sqlFoodcount);
                    $foodCount= mysqli_num_rows($resultFoodCount) ;
   
                    if(isset($_SESSION['login_user1'])){
                        $Uname=$_SESSION['login_user1'];
                        $r_id;
                        $r_name;
                        $sqlType="Select resto_id,fullname from restaurants where username='$Uname'";
                        $result1 = mysqli_query($conn, $sqlType);
                        if (mysqli_num_rows($result1) > 0)
                        {
                        while($row = mysqli_fetch_assoc($result1)){
                            $r_id=$row["resto_id"];
                            $r_name=$row["fullname"];
                        }
                        }
    
                        $sql = "SELECT * FROM food WHERE options = 'Enable' and resto_id='$r_id' ORDER BY food_id";
                        $result = mysqli_query($conn, $sql);
                        $foodCountRes=  mysqli_num_rows($result);
                   ?>
              <h3 style="text-align: center;color: #ff6900;margin-top: 1rem">Restaurant <?php echo $r_name;?></h3>
              
              <!-- Display all Food from food table -->
                <?php
                }
                else if (isset($_SESSION['login_user2'])) {
                    $Uname=$_SESSION['login_user2'];
                    $type;
                    $name_user;
                    $sqlType="Select food_pref,fullname from user where username='$Uname'";
                    $result1 = mysqli_query($conn, $sqlType);
                    if (mysqli_num_rows($result1) > 0)
                    {
                    while($row = mysqli_fetch_assoc($result1)){
                        $type=$row["food_pref"];
                        $name_user=$row["fullname"];
                    }
                    }
    
                        $sql = "SELECT * FROM food WHERE options = 'Enable' and food_type='$type' ORDER BY food_id";
                        $result = mysqli_query($conn, $sql);
                        $foodCount=  mysqli_num_rows($result);
    
                  ?>
                 <h3 style="text-align: center;margin-top: 1rem;color: green;text-transform: capitalize ">Hi <?php echo $name_user;?></h3>
                  <?php if($foodCount >0){?>
                     <h4 style="text-align: center;">You can order Food from your choice..!<br></h4>
                 <?php
                 }
                }
            else {
            echo "<h3 style='text-align:center;margin-top:1rem;'>Food</h3>";
            $sql = "SELECT * FROM food WHERE options = 'Enable' ORDER BY food_id";
            $result = mysqli_query($conn, $sql);
            }
            if (mysqli_num_rows($result) > 0)
            {
             $count=0;

           while($row = mysqli_fetch_assoc($result)){
                    if ($count == 0)
                        echo "<div class='row' style='margin-top:2rem;'>";
                        $r_id=$row["resto_id"];
                        $sql_R_id = "SELECT fullname FROM restaurants WHERE resto_id='$r_id'";
                        $result_rName = $conn->query($sql_R_id);
                        if (mysqli_num_rows($result_rName) > 0) {
                        while($row1 = $result_rName->fetch_assoc()) {
                        $R_Name=$row1["fullname"];
                 }
      
            }

        ?>
  <div class="col-md-3">
      <form method="post" action="orders.php?action=add&id=<?php echo $row['food_id']; ?>&Rid=<?php echo $row['resto_id']; ?>">
       <div class="mypanel card" align="center" style="border: 1px solid lightgrey;">
            <img src="<?php echo $row["images_path"]; ?>" style="height: 150px;width:100%" class="img-responsive">
            <h4 class="text-dark"><?php echo $R_Name; ?></h4>
            <h4 class="text-dark"><?php echo $row["food_name"]; ?></h4>
            <h5 class="text-info"><?php echo $row["food_desc"]; ?></h5>
            <h5 class="text-danger">&#8377; <?php echo $row["food_price"]; ?>/-</h5>
            <h5 class="text-info">Quantity: <input type="number" min="1" max="25" name="quantity" class="form-control" value="1" style="width: 60px;"> </h5>
            <input type="hidden" name="hidden_name" value="<?php echo $row["food_name"]; ?>">
            <input type="hidden" name="hidden_price" value="<?php echo $row["food_price"]; ?>">
            <input type="hidden" name="hidden_RID" value="<?php echo $row["resto_id"]; ?>">
            <button type="submit" name="add" style="margin-top:5px; margin-bottom: 5px"
                data-toggle="modal" data-target="#orderModal"
                <?php if (isset($_SESSION['login_user1'])){ ?> disabled <?php   } ?> 
                class="btn btn-success" value="Order">Order</button>
        </form>
       
     </div>
   </div>

        <?php
        $count++;
        if($count==4)
        {
        echo "</div>";
        $count=0;
        }
        }
        ?>

    </div>
</div>
    <?php
    }
    else
    {
    ?>
<!-- Messge displayed when no food is available -->
   <div class="container" style="margin-top: 3rem;margin-bottom: 3rem">
      <div class="">
        <center>
            <?php if($foodCountRes==0 && isset($_SESSION['login_user1'])){ ?>
            <label style="margin-left: 5px;color: red;"> <h1>No food available in Restaurant! You have to add it.</h1> </label>
            <p>Add Food Items...!</p><?php }?>
            <?php if($foodCount==0 && isset($_SESSION['login_user2'])){?>
            <label style="margin-left: 5px;color: red;"> <h1>Sorry for inconvience! Food is not available.</h1> </label>
            <?php }?>
        </center>
       
      </div>
     </div>
     <!-- Message div end here -->
</div>
<?php

}

?>
</body>
</html>