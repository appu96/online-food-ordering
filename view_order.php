<?php
session_start();
require 'connection.php';
$conn = Connect();
if(!isset($_SESSION['login_user1'])){
header("location: restoLogin.php"); 
}
?>

<html>
    <head>
        <title> Restaurants Order | The Sassy Spoon</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/user_orders.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script</script>
  </head>
  <body>
   <nav class="navbar navbar-inverse navbar-fixed-top navigation-clean-search" role="navigation">
       <div class="container">
           <div class="navbar-header">
                  <a class="navbar-brand" href="#" style="color:white;">The Sassy Spoon</a>
              </div>
               <div class="collapse navbar-collapse " id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li><a href="index.php">Home</a></li>
                    </ul>
                  <?php
                      if(isset($_SESSION['login_user1'])){
                    ?>
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" style="color:white"> Welcome <?php echo $_SESSION['login_user1']; ?> </a></li>
                    <li><a href="logout_resto.php"></span> Log Out </a></li>
                  </ul>
                   <?php
                     }
               else if (isset($_SESSION['login_user2'])) {
                ?>
                <ul class="nav navbar-nav navbar-right">
                      <li><a href="#"><span class="glyphicon glyphicon-user"></span> Welcome <?php echo $_SESSION['login_user2']; ?> </a></li>
                      <li><a href="logout_user.php"><span class="glyphicon glyphicon-log-out"></span> Log Out </a></li>
                </ul>
               <?php }?>
            </div>
         </div>
     </nav>
<!-- Header End Here -->
   <?php   
        $username=$_SESSION['login_user1'];
        $sqluser = "SELECT * FROM restaurants WHERE username = '$username' ";
        $resultUser = mysqli_query($conn, $sqluser);
        $c_id=0;
        
        if (mysqli_num_rows($resultUser) > 0)
        {
            while($rowuser = mysqli_fetch_assoc($resultUser)){
                $r_id =  $rowuser["resto_id"];
            }
         }
            $sqlOrders = "SELECT * FROM orders WHERE resto_id = '$r_id' ";
            $resultOrders = mysqli_query($conn, $sqlOrders);
    
            if (mysqli_num_rows($resultOrders) > 0)
               {
    ?>
  <div style="margin-top:10rem; margin-left:50rem;">
           <table class="content-table" style="margin-top: 1rem;">
           <thead>
             <tr>
                 <th>Food Name</th>
                 <th>Order Date</th>
                 <th>Cutomer</th> 
                 <th>Food Price</th>
             </tr>
           </thead>
      </tbody> 
      <!-- Display Details of orders to restaurants -->
      <?php
        while($roworders = mysqli_fetch_assoc($resultOrders)){
            //get customer name
            $cust_id=$roworders['user_id'];
            $cust_name;
            $sqlCustname = "SELECT fullname FROM user WHERE user_id = '$cust_id' ";
            $resultCustName = mysqli_query($conn, $sqlCustname);
            if (mysqli_num_rows($resultCustName) > 0)
             {
                while($rowcustname = mysqli_fetch_assoc($resultCustName)){
                $cust_name=$rowcustname['fullname'];
                }
              }
             echo "<tr><td>" .$roworders['foodname']."</td>
             <td>".$roworders['order_date']."</td>
             <td>".$cust_name."</td>
             <td>".$roworders['food_price']."</td>
             </tr>";
       
    }
    ?>
          </table>
  </div><?php
  } else{
  ?>
  <!-- Message is displayed if there is no order from restaurants -->
      <div  style="width: 100%;text-align: center;font-size: 2rem;height: 100%;color: black">
          <h2 style="margin-top: 10rem;font-size:2rem;text-align: center;color:white;">No Order Found for your restaurants!</h2>
      </div>
      <?php }?>
      
    </body>
</html>