<?php
session_start();
require 'connection.php';
$conn = Connect();
if(!isset($_SESSION['login_user2'])){
header("location: userlogin.php"); 
}
?>
<!-- Html code starts here -->
<html>
    <head>
        <title> My Orders| The Sassy Spoon </title>
  </head>

  <title> Your Orders | The Sassy Spoon</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/user_orders.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script</script>
  <body>
      <!-- Script tag  -->
      <script type="text/javascript">
            window.onscroll = function()
            {
              scrollFunction()
            };

          function scrollFunction(){
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
              document.getElementById("myBtn").style.display = "block";
            } else {
              document.getElementById("myBtn").style.display = "none";
            }
          }

          function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
          }
    </script>
    <!-- Script tag close -->
 <nav class="navbar navbar-inverse navbar-fixed-top navigation-clean-search" role="navigation">
      <div class="container">
          <div class="navbar-header">
             <a class="navbar-brand" style="color:white;" href="index.php">The Sassy Spoon</a>
          </div>

       <div class="collapse navbar-collapse " id="myNavbar">
            <ul class="nav navbar-nav">
               <li><a href="foodmenu.php">Food Menu</a></li>
            </ul>

<?php
if(isset($_SESSION['login_user1'])){

?>
       <ul class="nav navbar-nav navbar-right">
                <li><a href="#" style="color:white;">Welcome <?php echo $_SESSION['login_user1']; ?> </a></li>
                <li><a href="logout_resto.php">Log Out </a></li>
         </ul>
<?php
}
else if (isset($_SESSION['login_user2'])) {
  ?>
           <ul class="nav navbar-nav navbar-right">
               <li><a href="#" style="color:white;"> Welcome <?php echo $_SESSION['login_user2']; ?> </a></li>
               <li><a href="logout_user.php">Log Out </a></li>
          </ul>
  <?php        
} ?>
   </div>
  </div>
</nav>

 <?php   
     $username=$_SESSION['login_user2'];
     $sqluser = "SELECT * FROM user WHERE username = '$username' ";
     $resultUser = mysqli_query($conn, $sqluser);
     $c_id=0;

      if (mysqli_num_rows($resultUser) > 0)
      {
        while($rowuser = mysqli_fetch_assoc($resultUser)){
         $user_id =  $rowuser["user_id"];
        }
      }
     
      $sqlOrders = "SELECT * FROM orders WHERE user_id = '$user_id' ";
      $resultOrders = mysqli_query($conn, $sqlOrders);
 
      if (mysqli_num_rows($resultOrders) > 0)
      {
      ?>
      <div style="margin-top:10rem; margin-left:50rem;">
           <table class="content-table" style="margin-top: 1rem;">
           <thead>
             <tr>
                 <th>Food Name</th>
                 <th>Order Date</th> 
                 <th>Food Price</th>
             </tr>
           </thead>
      </tbody> 
 
<!-- Fetching Order Detail -->
       <?php
        $total=0;
        while($roworders = mysqli_fetch_assoc($resultOrders)){
           echo "<tr><td>".$roworders['foodname']."</td>
                 <td>".$roworders['order_date']."</td>
                 <td>".$roworders['food_price']."</td>
                 </tr>";
            $total=$total+$roworders['food_price'];
        }
        ?>
             <tr>
                 <td colspan="3"><span style="float:right;margin-right: 7rem"><b>Total Price:&nbsp;&nbsp;</b><?php echo $total;?></span></td>
             </tr>
      </tbody>
              </table>
      </div>
          <?php
          
      } else{
      ?>
      <!-- Message is displayed if you have not not ordered from restaurants -->
         <h2 style="margin-top: 10rem;font-size:2rem;text-align: center;color:white;">You Have Not Ordered any Food Yet..Please explore our food menu!</h2>
      <?php }?>

      
    </body>
</html>