<?php
session_start();
require 'connection.php';
$conn = Connect();

?>

<html>
   <head><title> Home | The Sassy Spooon</title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="css/index.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>
<!-- Header Start from here -->
    <nav class="navbar navbar-inverse navbar-fixed-top navigation-clean-search" role="navigation">
      <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php">The Sassy Spoon</a>
                </div>
          <div class="collapse navbar-collapse " id="myNavbar">
                <ul class="nav navbar-nav">
                    <li><a href="index.php">Home</a></li>
                </ul>

             <?php
             if(isset($_SESSION['login_user1'])){

                ?>
                <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Welcome <?php echo $_SESSION['login_user1']; ?> </a></li>
                        <li><a href="view_order.php"><span class="glyphicon glyphicon-log-out"></span> View Order</a></li>
                        <li><a href="add_food.php"><span class="glyphicon glyphicon-log-out"></span>Add Food</a></li>
                        <li><a href="logout_resto.php"><span class="glyphicon glyphicon-log-out"></span> Log Out </a></li>
                 </ul>
             <?php
                            }
                else if (isset($_SESSION['login_user2'])) {
                ?>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#"><span class="glyphicon glyphicon-user"></span> Welcome <?php echo $_SESSION['login_user2']; ?> </a></li>
                            <li><a href="user_orders.php"><span class="glyphicon glyphicon-shopping-cart">Your Order</span>
                            <li><a href="logout_user.php"><span class="glyphicon glyphicon-log-out"></span> Log Out </a></li>
                        </ul>
                <?php        
                }
          else {

          ?>

         <ul class="nav navbar-nav navbar-right">
               <li><a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Sign Up <span class="caret"></span> </a>
                    <ul class="dropdown-menu">
                        <li> <a href="usersignup.php"> User Sign-up</a></li>
                        <li> <a href="managersignup.php"> Manager Sign-up</a></li>
                       
                    </ul>
                </li>

                <li><a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-log-in"></span> Login <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li> <a href="userlogin.php"> User Login</a></li>
                    <li> <a href="restologin.php"> Manager Login</a></li>
                   
                </ul>
               </li>
        </ul>

        <?php
        }
        ?>
        
       </div>
    </div>
</nav>
<!-- Header end here -->

<!-- Welcome Div Starts -->
  
      <div style="text-align:center; padding: 35px; font-family: latha;  color: white;">
            <h1>Hi! <br> Welcome to The Sassy Spoon</h1>
            <br>
            <?php
             if(isset($_SESSION['login_user2'])){

                ?>
            <h3>Feeling Hungry?</h3>
            <center><a class="btn btn-success btn-lg" href="userlogin.php" role="button"> Order Now </a></center>
       </div>

       <?php
        }
        ?>
  <!-- Welcome div end -->

  </body>
</html>